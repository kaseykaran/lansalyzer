﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata.Ecma335;

namespace Lansalyzer
{
    internal class Program
    {
        /// <summary>
        ///
        /// </summary>
        /// <param name="args"></param>
        private static void Main(string[] args)
        {
            var lansaFileLines = System.IO.File.ReadAllLines(args[0]);

            var result = EnumerateFile(lansaFileLines.ToArray()).First();
            var className = System.IO.Path.GetFileNameWithoutExtension(args[0]);
            if (System.IO.Directory.Exists($"{className}"))
            {
                System.IO.Directory.Delete($"{className}", true);
            }

            System.IO.Directory.CreateDirectory($"{className}");
            System.IO.Directory.CreateDirectory($"{className}\\Routines");

            var routineNames = new List<string>();
            var routineDetails = new List<string>();
            routineDetails.Add("RoutineName,MapName,MapType,Class,Default,Help,Line");
            var stateDetails = new List<string>();

            var interfaceFile = new List<string>();
            var classFile = new List<string>();

            interfaceFile.AddRange(GenerateXmlField("remarks", StripComments(result.Comments).ToArray()));
            interfaceFile.Add($"public interface I{className}");
            interfaceFile.Add("{");
            interfaceFile.Add("");

            classFile.AddRange(GenerateXmlField("remarks", StripComments(result.Comments).ToArray()));
            classFile.Add($"public class {className} : I{className}");
            classFile.Add("{");
            classFile.Add("");
            foreach (var lansaObject in result.Objects)
            {
                if (lansaObject is Routine routine)
                {
                    var access = "public";
                    var isPublic = true;
                    if (!string.IsNullOrWhiteSpace(routine.Access))
                    {
                        access = "private";
                        isPublic = false;
                    }

                    if (isPublic)
                        interfaceFile.AddRange(GenerateXmlField("remarks", StripComments(routine.Comments).ToArray(), true));

                    classFile.AddRange(GenerateXmlField("remarks", StripComments(routine.Comments).ToArray(), true));

                    if (isPublic)
                        interfaceFile.Add(GenerateXmlField("summary", routine.Help, true));

                    classFile.Add(GenerateXmlField("summary", routine.Help, true));

                    var parameters = string.Empty;
                    var returnType = "void";

                    var name = lansaObject.Name;
                    if (lansaObject is EventRoutine eventRoutine)
                    {
                        name = "Event_" + eventRoutine.Handling.Trim('#').Replace('.', '_');
                    }
                    System.IO.File.WriteAllLines($"{className}\\Routines\\{name}.txt", lansaObject.Comments.Union(lansaObject.Lines));
                    routineNames.Add(name);
                    foreach (var map in routine.MappedObjects)
                    {
                        routineDetails.Add($"{name},{ map.Name},{map.MapType},{map.Class},{map.OptionalValue},{map.Help?.Replace(',', '/')},{map.Lines.FirstOrDefault() ?? "".Replace(',', '/')}");

                        if (map.MapType.ToUpperInvariant() == "*RESULT")
                        {
                            returnType = ConvertKnownLansaTypes(map.Class);

                            if (isPublic)
                                interfaceFile.Add(GenerateXmlField("returns", map.Help, true));

                            classFile.Add(GenerateXmlField("returns", map.Help, true));

                            continue;
                        }

                        if (isPublic)
                            interfaceFile.Add(GenerateXmlField($"param name=\"{map.Name}\"", map.Help, true));

                        classFile.Add(GenerateXmlField($"param name=\"{map.Name}\"", map.Help, true));

                        if (map.MapType.ToUpperInvariant() == "*BOTH")
                        {
                            parameters += "ref ";
                        }

                        if (map.MapType.ToUpperInvariant() == "*OUTPUT")
                        {
                            parameters += "out ";
                        }

                        parameters += $"{ConvertKnownLansaTypes(map.Class)} {map.Name} ";
                        if (!string.IsNullOrWhiteSpace(map.OptionalValue))
                        {
                            parameters += $"= {map.OptionalValue} ";
                        }

                        parameters = parameters.TrimEnd() + ", ";
                    }

                    parameters = parameters.TrimEnd().TrimEnd(',');
                    var line = $"{returnType} {name}({parameters})";
                    if (isPublic)
                    {
                        interfaceFile.Add($"\t{line};");
                        interfaceFile.Add("");
                    }

                    classFile.Add($"\t{line}");
                    classFile.Add("\t{");
                    classFile.Add("\t\tthrow new NotImplementedException();");
                    classFile.AddRange(routine.Lines.Select(x => $"\t\t//{x}"));
                    classFile.Add("\t}");
                    classFile.Add("");
                }
                else
                {
                }
            }
            interfaceFile.Add("}");
            classFile.Add("}");
            System.IO.File.WriteAllLines($"{className}\\RoutineNames.txt", routineNames);
            System.IO.File.WriteAllLines($"{className}\\RoutineMap.csv", routineDetails);
            System.IO.File.WriteAllLines($"{className}\\Routines\\_State.txt", result.Objects.Where(x => !x.GetType().IsSubclassOf(typeof(Routine))).SelectMany(x => x.Comments.Union(x.Lines).Union(new[] { "" })));
            System.IO.File.WriteAllLines($"{className}\\I{className}.cs", interfaceFile);
            System.IO.File.WriteAllLines($"{className}\\{className}.cs", classFile);
        }

        private static string ConvertKnownLansaTypes(string type)
        {
            switch (type.ToUpperInvariant())
            {
                case "#PRIM_BOLN":
                    return "bool";

                case "#PRIM_ALPH":
                case "#R_STRING":
                    return "string";

                case "#INT1":
                case "#INT2":
                case "#INT3":
                case "#INT4":
                case "#PRIM_NMBR":
                    return "int";

                case "#L_DVM":
                    return "decimal";
            }

            return type;
        }

        private static IEnumerable<string> GenerateXmlField(string field, string[] text, bool tab = false)
        {
            if (text.Length == 0) yield return $"{(tab ? "\t" : "")}/// <{field}></{CleanFieldName(field)}>";
            else if (text.Length == 1)
            {
                yield return $"{(tab ? "\t" : "")}/// <{field}>{text.First()}</{CleanFieldName(field)}>";
            }
            else
            {
                yield return $"{(tab ? "\t" : "")}/// <{field}>";
                foreach (var line in text)
                {
                    yield return $"{(tab ? "\t" : "")}/// {line}";
                }
                yield return $"{(tab ? "\t" : "")}/// </{CleanFieldName(field)}>";
            }
        }

        private static string GenerateXmlField(string field, string text, bool tab = false)
        {
            return $"{(tab ? "\t" : "")}/// <{field}>{text}</{CleanFieldName(field)}>";
        }

        private static string CleanFieldName(string name)
        {
            return name.Split(' ')[0];
        }

        private static IEnumerable<string> StripComments(IList<string> comments)
        {
            if (comments.Count == 0) yield break;
            foreach (var comment in comments)
            {
                var line = comment.Trim('*').Trim();
                if (string.IsNullOrWhiteSpace(line)) continue;

                yield return line;
            }
        }

        private static IEnumerable<LansaPart> EnumerateFile(string[] lansaFileLines)
        {
            LansaObject workingObject = null;
            LansaPart workingPart = null;
            var commentBuffer = new List<string>();
            State workingState = State.LookingForFunction;
            foreach (var line in lansaFileLines)
            {
                workingObject?.Lines.Add(line);
                workingPart?.Lines.Add(line);

                if (string.IsNullOrWhiteSpace(line))
                {
                    continue;
                }

                var searchLine = line.Trim();
                if (searchLine.StartsWith("*"))
                {
                    commentBuffer.Add(line);
                    continue;
                }

                var newObject = ObjectFactory(searchLine, workingState);
                if (newObject is LansaPart newLansaPart)
                {
                    newLansaPart.Comments = commentBuffer.ToArray().ToList();

                    workingPart = newLansaPart;
                    workingState = State.LookingForRoutine;
                }
                else if (newObject is LansaObject newLansaObject)
                {
                    newLansaObject.Comments = commentBuffer.ToArray().ToList();
                    commentBuffer.Clear();

                    if (newLansaObject is MapObject mapObject)
                    {
                        if (workingObject is Routine routine)
                        {
                            routine.MappedObjects.Add(mapObject);
                        }
                    }
                    else
                    {
                        workingObject = newLansaObject;
                        workingPart?.Objects.Add(workingObject);
                        if (workingObject is Routine)
                        {
                            workingState = State.InRoutine;
                        }
                    }

                    //Find group owned fields into the respective group
                    if (newLansaObject is LansaGroup group)
                    {
                        foreach (var groupField in group.Fields.ToArray())
                        {
                            foreach (var partObject in workingPart.Objects)
                            {
                                if (partObject is LansaField partField && (partField.Field == groupField.Field || partField.Name == groupField.Field))
                                {
                                    if (partField.Name == "#xg_all")
                                    {
                                    }
                                    partField.InGroup = true;
                                    group.Fields.Remove(groupField);
                                    group.Fields.Add(partField);
                                }
                            }
                        }
                    }

                    //Move group owned fields into the respective group
                    if (newLansaObject is LansaField field)
                    {
                        foreach (var obj in workingPart.Objects)
                        {
                            if (obj is LansaGroup innerGroup)
                            {
                                foreach (var innerField in innerGroup.Fields.ToArray())
                                {
                                    if (field.Field == innerField.Field || field.Field == innerField.Name)
                                    {
                                        if (field.Name == "#xg_all")
                                        {
                                        }
                                        field.InGroup = true;
                                        innerGroup.Fields.Remove(innerField);
                                        innerGroup.Fields.Add(field);
                                    }
                                }
                            }
                        }
                    }

                    //todo do post processing on the various objects here
                }
                else if (workingState == State.InRoutine && workingObject != null)
                {
                    var splitLine = searchLine.Split(' ');
                    if (splitLine[0].ToUpperInvariant() == TypeEndDictionary[workingObject.GetType()])
                    {
                        workingState = State.LookingForRoutine;
                        workingObject = null;
                        commentBuffer.Clear();
                    }
                }
                else if (workingState == State.LookingForFunction && workingPart != null)
                {
                    var splitLine = searchLine.Split(' ');
                    if (splitLine[0].ToUpperInvariant() == TypeEndDictionary[workingPart.GetType()])
                    {
                        workingState = State.LookingForFunction;
                        yield return workingPart;
                        workingObject = null;
                        workingPart = null;
                    }
                }

                if (!(workingObject is Routine))
                {
                    workingObject = null;
                }
            }

            if (workingPart != null)
            {
                yield return workingPart;
            }
        }

        private static object ObjectFactory(string line, State currentState)
        {
            var splitLine = line.Split(' ');
            var key = (splitLine[0].ToUpperInvariant(), currentState);
            if (TypeMapDictionary.ContainsKey(key))
            {
                var retObject = Activator.CreateInstance(TypeMapDictionary[key]);

                if (retObject is LansaObject lansaObject)
                {
                    FillLansaObject(lansaObject, line);
                }
                else if (retObject is LansaPart lansaPart)
                {
                    FillLansaPart(lansaPart, line);
                }

                return retObject;
            }

            return null;
        }

        private static void FillLansaPart(LansaPart lansaPart, string line)
        {
            lansaPart.Role = GetValue("ROLE", line, out var result) ? result : lansaPart.Role;
        }

        private static void FillLansaObject(LansaObject lansaObject, string line)
        {
            lansaObject.Lines.Add(line);
            lansaObject.Name = GetValue("NAME", line, out var result) ? result : lansaObject.Name;
            lansaObject.Help = GetValue("HELP", line, out result) ? result.Trim(new[] { ' ', '\'' }) : lansaObject.Help;

            if (lansaObject is LansaGroup lansaGroup)
            {
                lansaGroup.Fields = GetValue("FIELDS", line, out result) ? EnumerateFields(result).ToList() : lansaGroup.Fields;
            }

            if (lansaObject is LansaField lansaField)
            {
                lansaField.Field = GetValue("Field", line, out result) ? result : lansaField.Field;
                lansaField.Type = GetValue("TYPE", line, out result) ? result : lansaField.Type;
                lansaField.Desc = GetValue("Desc", line, out result) ? result : lansaField.Desc;
                lansaField.Length = GetValue("Length", line, out result) ? int.TryParse(result, out var len) ? len : (int?)null : lansaField.Length;
            }

            if (lansaObject is LansaList lansaList)
            {
                lansaList.Counter = GetValue("Counter", line, out result) ? EnumerateFields(result).FirstOrDefault() : lansaList.Counter;
                lansaList.Entries = GetValue("Entries", line, out result) ? result : lansaList.Entries;
            }

            if (lansaObject is LansaProperty lansaProperty)
            {
                lansaProperty.Getter = GetValue("Get", line, out result) ? EnumerateObjects(result).LastOrDefault() : lansaProperty.Getter;
                lansaProperty.Setter = GetValue("Set", line, out result) ? EnumerateObjects(result).LastOrDefault() : lansaProperty.Setter;
            }

            if (lansaObject is MapObject mapObject)
            {
                mapObject.MapType = GetValue("For", line, out result) ? result : mapObject.MapType;
                mapObject.OptionalValue = GetValue("MANDATORY", line, out result) ? result : mapObject.OptionalValue;
            }

            if (lansaObject is LansaVariable lansaVariable)
            {
                lansaVariable.Class = GetValue("CLASS", line, out result) ? result : lansaVariable.Class;
                lansaVariable.DefaultValue = GetValue("Value", line, out result) ? result : lansaVariable.DefaultValue;
            }

            if (lansaObject is EventRoutine eventRoutine)
            {
                eventRoutine.Handling = GetValue("Handling", line, out result) ? result : eventRoutine.Handling;
            }

            if (lansaObject is Routine routine)
            {
                routine.Access = GetValue("Access", line, out result) ? result : routine.Access;
            }
        }

        private static IEnumerable<LansaObject> EnumerateObjects(string objectName)
        {
            foreach (var field in objectName.Split(' '))
            {
                if (!string.IsNullOrWhiteSpace(field))
                {
                    yield return new LansaObject() { Name = field };
                }
            }
        }

        private static IEnumerable<LansaField> EnumerateFields(string fieldsString)
        {
            foreach (var field in fieldsString.Split(' '))
            {
                if (!string.IsNullOrWhiteSpace(field))
                {
                    yield return new LansaField() { Field = field };
                }
            }
        }

        private static bool GetValue(string name, string line, out string value)
        {
            try
            {
                var lineUpper = line.ToUpperInvariant();
                var nameUpper = name.ToUpperInvariant();
                if (!lineUpper.Contains(nameUpper))
                {
                    value = String.Empty;
                    return false;
                }

                var location = lineUpper.IndexOf(nameUpper, StringComparison.Ordinal);
                var start = lineUpper.IndexOf('(', location);
                var endOfName = location + name.Length;
                var between = line.Substring(endOfName, start - endOfName);
                if (!string.IsNullOrWhiteSpace(between))
                {
                    value = String.Empty;
                    return false;
                }
                var end = lineUpper.IndexOf(')', start);
                value = line.Substring(start + 1, end - start - 1);
                return true;
            }
            catch
            {
                value = string.Empty;
                return false;
            }
        }

        public static Dictionary<(string, State), Type> TypeMapDictionary { get; } = new Dictionary<(string, State), Type>()
        {
            {("BEGIN_COM", State.LookingForFunction), typeof(LansaPart) },
            {("MTHROUTINE", State.LookingForRoutine), typeof(MethodRoutine) },
            {("PTYROUTINE", State.LookingForRoutine), typeof(PropertyRoutine) },
            {("EVTROUTINE", State.LookingForRoutine), typeof(EventRoutine) },
            {("DEFINE_MAP", State.InRoutine), typeof(MapObject) },
            {("DEFINE_COM", State.LookingForRoutine), typeof(LansaVariable) },
            {("DEF_LIST", State.LookingForRoutine), typeof(LansaList) },
            {("GROUP_BY", State.LookingForRoutine), typeof(LansaGroup) },
            {("DEFINE", State.LookingForRoutine), typeof(LansaField) },
            {("DEFINE_PTY", State.LookingForRoutine), typeof(LansaProperty) },
        };

        public static Dictionary<Type, string> TypeEndDictionary { get; } = new Dictionary<Type, string>()
        {
            {typeof(LansaPart),"END_COM" },
            {typeof(Routine),"ENDROUTINE" },
            {typeof(EventRoutine),"ENDROUTINE" },
            {typeof(MethodRoutine),"ENDROUTINE" },
            {typeof(PropertyRoutine),"ENDROUTINE" }
        };
    }

    public class LansaGroup : LansaField
    {
        public List<LansaField> Fields { get; set; }
    }

    public class LansaField : LansaObject
    {
        public int? Length { get; set; }
        public string Desc { get; set; }

        public string Type { get; set; }

        public string Field { get; set; }

        public bool InGroup { get; set; }
    }

    public class LansaList : LansaGroup
    {
        public string Entries { get; set; }
        public LansaField Counter { get; set; }
    }

    public class LansaPart
    {
        public List<LansaObject> Objects { get; } = new List<LansaObject>();
        public List<string> Lines { get; } = new List<string>();
        public List<string> Comments { get; set; }
        public string Role { get; set; }
    }

    public class LansaObject
    {
        public string Name { get; set; }
        public List<string> Comments { get; set; }
        public string Help { get; set; }
        public List<string> Lines { get; } = new List<string>();
    }

    public class LansaVariable : LansaObject
    {
        public string Class { get; set; }

        public string DefaultValue { get; set; }
    }

    public class LansaProperty : LansaObject
    {
        public LansaObject Setter { get; set; }
        public LansaObject Getter { get; set; }
    }

    public class MapObject : LansaVariable
    {
        public string MapType { get; set; }
        public string OptionalValue { get; set; }
    }

    public class Routine : LansaObject
    {
        public List<MapObject> MappedObjects { get; } = new List<MapObject>();
        public string Access { get; set; }
    }

    public class EventRoutine : Routine
    {
        public string Handling { get; set; }
    }

    public class PropertyRoutine : Routine
    {
    }

    public class MethodRoutine : Routine
    {
    }

    public enum State
    {
        LookingForFunction,
        LookingForRoutine,
        InRoutine
    }
}